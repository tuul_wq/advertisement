package DAO;

import Classes.Rating;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;


public class RatingDAO {
    
    public List<Rating> select(){
        List<Rating> lr = new LinkedList<>();
        try {
         Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/advert_test?user=root&password=root");
         PreparedStatement stmt = conn.prepareStatement( "SELECT * FROM ratings");
         ResultSet result = stmt.executeQuery();
     
         while(result.next()){
                Rating r = new Rating();
                r.setId(result.getInt(1));
                r.setRate(result.getInt(2));
                lr.add(r);
            }
         conn.close();
        } catch (SQLException ex) {System.out.println(ex); }  
        
        return lr;
    }          
}
