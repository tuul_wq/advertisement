package DAO;


import Classes.Model;
import Classes.ModelYear;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;



public class ModelYearDAO {
    
    public List<ModelYear> select(List<Model> lm){        
        List<ModelYear> lmy = new LinkedList<>();
        
        try {
         Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/advert_test?user=root&password=root");
         PreparedStatement stmt = conn.prepareStatement( "SELECT * FROM modelyears");
         ResultSet result = stmt.executeQuery();         
         
         while(result.next()){
                ModelYear my = new ModelYear();
                my.setId(result.getInt(1));
                my.setYear(result.getDate(2));                
                
                int i = 0;
                boolean flag = true;                
                while(flag){
                    if(lm.get(i).getId() == result.getInt(3)){
                        my.setMdl(lm.get(i));
                        flag = false;
                    }
                    i++;
                }               
            }
         conn.close();
        } catch (SQLException ex) {System.out.println(ex); }  
        
        return lmy;
    }          
}
