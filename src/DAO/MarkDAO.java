package DAO;

import Classes.Mark;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;



public class MarkDAO {
   
    public List<Mark> select(){
        
        List<Mark> lm = new LinkedList<>();        
        try {
         Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/advert_test?user=root&password=root");
         PreparedStatement stmt = conn.prepareStatement( "SELECT * FROM marks");
         ResultSet result = stmt.executeQuery();
      
         while(result.next()){
                Mark mrk = new Mark();
                mrk.setId(result.getInt(1));
                mrk.setMark(result.getString(2));                
                lm.add(mrk);
            }
         conn.close();
        } catch (SQLException ex) {System.out.println(ex); }  
        
        return lm;
    }          
}
