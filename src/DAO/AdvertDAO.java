
package DAO;

import Classes.Advertisement;
import Classes.Model;
import Classes.ModelYear;
import Classes.Quality;
import Classes.User;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class AdvertDAO {    
    
    private void addModelYearID(List<ModelYear> lmy, int item, Advertisement ad){
        int i = 0;
                boolean flag = true;                
                while(flag){
                    if(lmy.get(i).getId() == item ){
                        ad.setMy(lmy.get(i));
                        flag = false;
                    }
                    i++;
                }
    }
    
    private void addQualityID(List<Quality> lq, int item, Advertisement ad){
        int i = 0;
                boolean flag = true;                
                while(flag){
                    if(lq.get(i).getId() == item ){
                        ad.setQ(lq.get(i));
                        flag = false;
                    }
                    i++;
                }
    }
    
    private void addUserID(List<User> lu, int item, Advertisement ad){
        int i = 0;
                boolean flag = true;                
                while(flag){
                    if(lu.get(i).getId() == item ){
                        ad.setUs(lu.get(i));
                        flag = false;
                    }
                    i++;
                }
    }
    
    
    public List<Advertisement> select(List<ModelYear> lmy, List<Quality> lq, List<User> lu){
        
        List<Advertisement> la = new LinkedList<>();
        
        try {             
         Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/advert_test?user=root&password=root");
         PreparedStatement stmt = conn.prepareStatement( "SELECT * FROM advertisements");
         ResultSet result = stmt.executeQuery();     
         
         while(result.next()){
                Advertisement ad = new Advertisement();
                
                ad.setId(result.getInt(1));
                ad.setDate(result.getDate(2));
                ad.setDeleted(result.getDate(3));
                ad.setPrice(result.getInt(4));
                ad.setColor(result.getString(5));
                ad.setRun(result.getInt(6));
                addModelYearID(lmy,result.getInt(7), ad);
                addQualityID(lq,result.getInt(8), ad);
                addUserID(lu,result.getInt(9), ad);              
              
                la.add(ad);
            }
         conn.close();
        } catch (SQLException ex) {System.out.println(ex); }        
        return la;
    }        
        
 
    public void insert(Advertisement ad, Model m, User u, Quality q){        
        try {
         Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/advert_test?user=root&password=root");
         PreparedStatement stmt = conn.prepareStatement(
                 "INSERT into advertisements(date, deleted, price, color,"
                 + " run, id_model_year, id_quality, id_user) values(?, ?, ?, ?, ?, ?, ?, ?)");         
            
         stmt.setDate(1, (java.sql.Date)ad.getDate());
         stmt.setDate(2, (java.sql.Date)ad.getDeleted());
         stmt.setInt(3, ad.getPrice());
         stmt.setString(4, ad.getColor());
         stmt.setInt(5, ad.getRun());
         stmt.setInt(6, m.getId());
         stmt.setInt(7, q.getId());
         stmt.setInt(8, u.getId());          
         stmt.execute();
         
         conn.close();
            
    } catch (SQLException ex) { System.out.println(ex); }    
    }  
}
    

