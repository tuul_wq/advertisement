
package DAO;

import Classes.Advertisement;
import Classes.Rating;
import Classes.User;
import Classes.UserRating;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class UserRatingDAO {
    
    public void addUserID(List<User> lu, int item, UserRating ur){
    
        int i = 0;
                boolean flag = true;                
                while(flag){
                    if(lu.get(i).getId() == item ){
                        ur.setUs(lu.get(i));
                        flag = false;
                    }
                    i++;
                }       
        
    }
    
    public void addRatingID(List<Rating> lr, int item, UserRating ur){
    
        int i = 0;
                boolean flag = true;                
                while(flag){
                    if(lr.get(i).getId() == item ){
                        ur.setRt(lr.get(i));
                        flag = false;
                    }
                    i++;
                }       
        
    }
    
    public void addAdvertID(List<Advertisement> la, int item, UserRating ur){
    
        int i = 0;
                boolean flag = true;                
                while(flag){
                    if(la.get(i).getId() == item ){
                        ur.setAd(la.get(i));
                        flag = false;
                    }
                    i++;
                }       
        
    }
    
    
    public List<UserRating> select(List<User> lu, List<Rating> lr, List<Advertisement> la){  
        
        List<UserRating> lur = new LinkedList<>();
        
        try {
         Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/advert_test?user=root&password=root");
         PreparedStatement stmt = conn.prepareStatement( "SELECT * FROM userratings");
         ResultSet result = stmt.executeQuery();
         
         
         while(result.next()){
                UserRating ur = new UserRating();
                
                ur.setId(result.getInt(1));                
                addUserID(lu,result.getInt(2),ur);
                addRatingID(lr,result.getInt(3),ur);            
                addAdvertID(la,result.getInt(4),ur);   
                        
                lur.add(ur);
            }
         conn.close();
        } catch (SQLException ex) {System.out.println(ex); } 
        
        return lur;
    }
    
}
