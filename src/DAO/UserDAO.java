
package DAO;

import Classes.Quality;
import Classes.Rating;
import Classes.User;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class UserDAO {   
    
    public List<User> select(){        
        
        List<User> lu = new LinkedList<>();
        
        try {
         Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/advert_test?user=root&password=root");
         PreparedStatement stmt = conn.prepareStatement( "SELECT * FROM users");
         ResultSet result = stmt.executeQuery();
        
         while(result.next()){
                User u = new User();
                u.setId(result.getInt(1));                
                u.setEmail(result.getString(2));
                u.setName(result.getString(4));
                lu.add(u);
            }
         conn.close();
        } catch (SQLException ex) {System.out.println(ex); }  
        
        return lu;
    }          
    
    
    public void insert(User u){
        try {
         Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/advert_test?user=root&password=root");
         PreparedStatement stmt = conn.prepareStatement(
                 "INSERT into users(email, password, name) values(?, ?, ?)");         
            
         stmt.setString(1, u.getEmail());
         stmt.setString(2, u.getPassword());
         stmt.setString(3, u.getName());                                        
         stmt.execute();
         
         conn.close();
            
    } catch (SQLException ex) { System.out.println(ex); }    
    }  
    
 }
    

