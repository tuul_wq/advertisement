package DAO;


import Classes.Mark;
import Classes.Model;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;



public class ModelDAO {
  
    public List<Model> select(List<Mark> lmark){
        List<Model> lm = new LinkedList<>();
        
        try {
         Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/advert_test?user=root&password=root");
         PreparedStatement stmt = conn.prepareStatement( "SELECT * FROM models");
         ResultSet result = stmt.executeQuery();         
         
         while(result.next()){
                Model mdl = new Model();
                mdl.setId(result.getInt(1));
                mdl.setModel(result.getString(2));
                
                int i = 0;
                boolean flag = true;                
                while(flag){
                    if(lmark.get(i).getId() == result.getInt(3)){
                        mdl.setMrk(lmark.get(i));
                        flag = false;
                    }
                    i++;
                }
                
                lm.add(mdl);
            }
         conn.close();
        } catch (SQLException ex) {System.out.println(ex); }  
        
        return lm;
    }          
}
