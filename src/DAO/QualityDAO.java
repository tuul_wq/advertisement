package DAO;

import Classes.Quality;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;


public class QualityDAO {
    
    public List<Quality> select(){
        
        List<Quality> lq = new LinkedList<>();
        
        try {
         Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/advert_test?user=root&password=root");
         PreparedStatement stmt = conn.prepareStatement( "SELECT * FROM qualities");
         ResultSet result = stmt.executeQuery();
        
         while(result.next()){
                Quality q = new Quality();
                q.setId(result.getInt(1));
                q.setQuality(result.getString(2));                
                lq.add(q);
            }
         conn.close();
        } catch (SQLException ex) {System.out.println(ex); }  
        
        return lq;
    }          
}
