
package Classes;

public class Model {
    
    private int id;
    private String model;
    
    private Mark mrk;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Mark getMrk() {
        return mrk;
    }

    public void setMrk(Mark mrk) {
        this.mrk = mrk;
    }
   
}
