
package Classes;

public class UserRating {
    
    private int id;
    
    private User us;
    private Rating rt;
    private Advertisement ad;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUs() {
        return us;
    }

    public void setUs(User us) {
        this.us = us;
    }

    public Rating getRt() {
        return rt;
    }

    public void setRt(Rating rt) {
        this.rt = rt;
    }

    public Advertisement getAd() {
        return ad;
    }

    public void setAd(Advertisement ad) {
        this.ad = ad;
    }
    
}
